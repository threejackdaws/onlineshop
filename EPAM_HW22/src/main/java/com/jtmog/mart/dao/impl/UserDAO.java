package com.jtmog.mart.dao.impl;

import com.jtmog.mart.dao.GlobalDAO;
import com.jtmog.mart.entity.User;
import com.jtmog.mart.util.JPAUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

public class UserDAO implements GlobalDAO<User, String> {

    private EntityManager entityManager;

    @Override
    public void insert(User model) {
        entityManager = JPAUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();

        User user = new User(model.getLogin());
        entityManager.persist(user);

        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public User read(String login) {
        entityManager = JPAUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();

        Query query = entityManager.createQuery("SELECT u FROM User AS u WHERE u.login=:login");
        query.setParameter("login", login);

        User singleResult;
        try {
            singleResult = (User) query.getSingleResult();
        } catch (Exception e) {
            return null;
        } finally {
            entityManager.getTransaction().commit();
            entityManager.close();
        }
        return singleResult;
    }

    @Override
    public void update(User model) {
        entityManager = JPAUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();

        User user = entityManager.find(User.class, model.getId());

        user.setLogin(model.getLogin());
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void delete(User model) {
        entityManager = JPAUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();

        entityManager.remove(model);

        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
