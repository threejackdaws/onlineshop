package com.jtmog.mart.dao.impl;

import com.jtmog.mart.dao.GlobalDAO;
import com.jtmog.mart.entity.Good;
import com.jtmog.mart.util.JPAUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

public class GoodDAO implements GlobalDAO<Good, String> {

    private EntityManager entityManager;

    @Override
    public void insert(Good model) {
        entityManager = JPAUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();

        Good good = new Good(model.getTitle(), model.getPrice());
        entityManager.persist(good);

        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public Good read(long goodId) {
        entityManager = JPAUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();

        Good result = entityManager.find(Good.class, goodId);

        entityManager.getTransaction().commit();
        entityManager.close();

        return result;
    }

    public Good read(String title) {
        entityManager = JPAUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();

        Query query = entityManager.createQuery("SELECT g FROM Good AS g WHERE g.title=:title");
        query.setParameter("title", title);
        Good singleResult = (Good) query.getSingleResult();

        entityManager.getTransaction().commit();
        entityManager.close();

        return singleResult;
    }

    @Override
    public void update(Good model) {
        entityManager = JPAUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();

        Good good = entityManager.find(Good.class, model.getId());

        good.setPrice(model.getPrice());
        good.setTitle(model.getTitle());
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void delete(Good model) {
        entityManager = JPAUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();

        entityManager.remove(model);

        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public List<Good> getAll() {
        entityManager = JPAUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();

        Query query = entityManager.createQuery("SELECT g FROM Good g");
        List<Good> listResult = (List<Good>) query.getResultList();

        entityManager.getTransaction().commit();
        entityManager.close();

        return listResult;
    }
}