package com.jtmog.mart.dao;

public interface GlobalDAO<K, V> {
    void insert(K model);

    K read(V key);

    void update(K model);

    void delete(K model);
}
