package com.jtmog.mart.dao.impl;

import com.jtmog.mart.dao.GlobalDAO;
import com.jtmog.mart.entity.Order;
import com.jtmog.mart.util.JPAUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

public class OrderDAO implements GlobalDAO<Order, Long> {

    private EntityManager entityManager;

    @Override
    public void insert(Order model) {
        entityManager = JPAUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();

        Order order = new Order(model.getUserId());
        entityManager.persist(order);

        entityManager.getTransaction().commit();
        entityManager.close();

    }

    @Override
    public Order read(Long userId) {
        entityManager = JPAUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();

        Order result = entityManager.find(Order.class, userId);

        entityManager.getTransaction().commit();
        entityManager.close();

        return result;
    }

    @Override
    public void update(Order model) {
        entityManager = JPAUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();

        Query query = entityManager.createQuery("SELECT o FROM Order AS o WHERE o.userId=:userId");
        query.setParameter("userId", model.getUserId());
        Order singleResult = (Order) query.getSingleResult();

        singleResult.setTotalPrice(model.getTotalPrice());
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void delete(Order model) {
        entityManager = JPAUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();

        entityManager.remove(model);

        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
