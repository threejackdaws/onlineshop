package com.jtmog.mart.dao.impl;

import com.jtmog.mart.dao.GlobalDAO;
import com.jtmog.mart.entity.OrderGood;
import com.jtmog.mart.util.JPAUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

public class OrderGoodDAO implements GlobalDAO<OrderGood, Long> {

    private EntityManager entityManager;

    @Override
    public void insert(OrderGood model) {
        entityManager = JPAUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();

        OrderGood orderGood = new OrderGood(model.getOrderId(), model.getGoodId());
        entityManager.persist(orderGood);

        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public OrderGood read(Long orderId) {
        entityManager = JPAUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();

        Query query = entityManager.createQuery("SELECT og FROM OrderGood AS og WHERE og.orderId=:orderId");
        query.setParameter("orderId", orderId);
        OrderGood singleResult = (OrderGood) query.getSingleResult();

        entityManager.getTransaction().commit();
        entityManager.close();

        return singleResult;
    }

    @Override
    public void update(OrderGood model) {
        throw new UnsupportedOperationException("You can't UPDATE.");
    }

    @Override
    public void delete(OrderGood model) {
        entityManager = JPAUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();

        entityManager.remove(model);

        entityManager.getTransaction().commit();
        entityManager.close();

    }

    public List<OrderGood> getAllByOrderId(long orderId) {
        entityManager = JPAUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();

        Query query = entityManager.createQuery("SELECT og FROM OrderGood og WHERE og.orderId=:orderId");
        query.setParameter("orderId", orderId);

        List<OrderGood> listResult = (List<OrderGood>) query.getResultList();

        entityManager.getTransaction().commit();
        entityManager.close();

        return listResult;
    }
}
