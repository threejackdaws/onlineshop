package com.jtmog.mart.util;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil implements AutoCloseable {
    private static EntityManagerFactory factory;


    public static EntityManagerFactory getEntityManagerFactory() {
        if (factory == null) {
            factory = Persistence.createEntityManagerFactory("EntityManager");
        }
        return factory;
    }

    @Override
    public void close() {
        if(factory != null) {
            factory.close();
        }
    }
}
