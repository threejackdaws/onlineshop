package com.jtmog.mart.listener;

import com.jtmog.mart.dao.impl.GoodDAO;
import com.jtmog.mart.entity.Good;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.LinkedList;
import java.util.List;

@WebListener()
public class ApplicationListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        GoodDAO goodDAO = new GoodDAO();

        List<Good> goods = new LinkedList<>();
        goods.add(new Good("Petryshka", 3));
        goods.add(new Good("Gorilka", 87));
        goods.add(new Good("Borsh", 20));
        goods.add(new Good("Salo", 20));

        for (Good good : goods) {
            goodDAO.insert(good);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
