package com.jtmog.mart.servise.impl;

import com.jtmog.mart.dao.impl.GoodDAO;
import com.jtmog.mart.entity.Good;
import com.jtmog.mart.servise.GoodService;
import org.apache.log4j.Logger;

import java.util.List;

public class GoodServiceImpl implements GoodService {
    private static final Logger logger = Logger.getLogger(GoodServiceImpl.class);

    @Override
    public Good getGoodByTitle(String title) {
        Good good = null;
        GoodDAO goodDAO = new GoodDAO();
        good = goodDAO.read(title);
        return good;
    }

    @Override
    public Good getGoodById(long goodId) {
        Good good = null;
        GoodDAO goodDAO = new GoodDAO();
        good = goodDAO.read(goodId);
        return good;
    }

    @Override
    public List<Good> getAllGood() {
        List<Good> result = null;
        GoodDAO goodDAO = new GoodDAO();
        result = goodDAO.getAll();
        return result;
    }

    @Override
    public GoodService createListOfGoods() {
        return new GoodServiceImpl();
    }
}
