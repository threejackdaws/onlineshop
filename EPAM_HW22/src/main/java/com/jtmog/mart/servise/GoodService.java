package com.jtmog.mart.servise;

import com.jtmog.mart.entity.Good;

import java.util.List;

public interface GoodService {
    Good getGoodByTitle(String title);

    Good getGoodById(long goodId);

    List<Good> getAllGood();

    GoodService createListOfGoods();
}
