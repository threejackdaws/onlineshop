package com.jtmog.mart.servise;

import com.jtmog.mart.entity.Order;

import javax.servlet.http.HttpServletRequest;

public interface OrderServise {
    void createOrder(long userId);

    Order getOrderByUserId(long userId);

    void updateOrder(long userId, long totalPrice);

    void updateTotalPriceInOrder(HttpServletRequest req);

    void readTotalPriceFromOrder(HttpServletRequest req);
}
