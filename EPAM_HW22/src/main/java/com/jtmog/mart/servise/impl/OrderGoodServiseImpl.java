package com.jtmog.mart.servise.impl;

import com.jtmog.mart.dao.impl.OrderGoodDAO;
import com.jtmog.mart.entity.Good;
import com.jtmog.mart.entity.OrderGood;
import com.jtmog.mart.servise.GoodService;
import com.jtmog.mart.servise.OrderGoodServise;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

public class OrderGoodServiseImpl implements OrderGoodServise {
    private static final Logger logger = Logger.getLogger(OrderGoodServiseImpl.class);

    private static final String GOOD_ID = "goodId";
    private static final String ORDER_ID = "orderId";
    private static final String BUCKET = "bucket";

    @Override
    public void createOrderGood(long orderId, long goodId) {
            OrderGoodDAO orderGoodDAO = new OrderGoodDAO();
            OrderGood orderGood = new OrderGood(orderId, goodId);
            orderGoodDAO.insert(orderGood);
    }

    @Override
    public List<OrderGood> getAllGoodByOrderId(long orderId) {
        List<OrderGood> result = null;
            OrderGoodDAO orderGoodDAO = new OrderGoodDAO();
            result = orderGoodDAO.getAllByOrderId(orderId);
        return result;
    }

    @Override
    public void createOrderGood(HttpServletRequest req) {
        Long orderId = (Long) req.getSession().getAttribute(ORDER_ID);
        Long goodId = Long.parseLong(req.getParameter(GOOD_ID));

        OrderGoodServise orderGoodServise = new OrderGoodServiseImpl();
        orderGoodServise.createOrderGood(orderId, goodId);
    }

    @Override
    public void readOrderGoodByOrderId(HttpServletRequest req) {
        Long orderId = (Long) req.getSession().getAttribute(ORDER_ID);

        OrderGoodServise orderGoodServise = new OrderGoodServiseImpl();
        List<OrderGood> allGoodByOrderId = orderGoodServise.getAllGoodByOrderId(orderId);

        List<Good> listGoodByOrderGood = getListGoodByOrderGood(allGoodByOrderId);
        req.setAttribute(BUCKET, listGoodByOrderGood);
    }

    private List<Good> getListGoodByOrderGood(List<OrderGood> list) {
        List<Long> goodIdList = new ArrayList<>();
        for (OrderGood orderGood : list) {
            long goodId = orderGood.getGoodId();
            goodIdList.add(goodId);
        }

        List<Good> goodList = new ArrayList<>();
        GoodService goodService = new GoodServiceImpl();
        for (Long goodId : goodIdList) {
            Good goodById = goodService.getGoodById(goodId);
            goodList.add(goodById);
        }
        return goodList;
    }
}
