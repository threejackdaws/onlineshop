package com.jtmog.mart.servise.impl;

import com.jtmog.mart.dao.impl.UserDAO;
import com.jtmog.mart.entity.Order;
import com.jtmog.mart.entity.User;
import com.jtmog.mart.servise.OrderServise;
import com.jtmog.mart.servise.UserServise;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class UserServiseImpl implements UserServise {
    private static final Logger logger = Logger.getLogger(UserServiseImpl.class);

    private static final String USER_ID = "userId";
    private static final String ORDER_ID = "orderId";

    @Override
    public void createUser(String login) {
        UserDAO userDAO = new UserDAO();
        User user = new User(login);
        userDAO.insert(user);
    }

    @Override
    public void createUser(String login, String password) {
        throw new UnsupportedOperationException("Not yet.");
    }

    @Override
    public User getUserByLogin(String login) {
        User user = null;
        UserDAO userDAO = new UserDAO();
        user = userDAO.read(login);
        return user;
    }

    @Override
    public void createUser(HttpServletRequest req, String clientName, String license) {
        HttpSession session = req.getSession();
        String login = req.getParameter(clientName);

        if (getUserByLogin(login) == null) {
            createUser(login);
            insertUserIdIntoOrderDB(req, getUserByLogin(login).getId());
        }
        User userByLogin = getUserByLogin(login);
        long userId = userByLogin.getId();
        session.setAttribute(USER_ID, userId);
        session.setAttribute(clientName, login);
        session.setAttribute(license, req.getParameter(license));

        OrderServise orderServise = new OrderServiceImpl();
        long orderId = orderServise.getOrderByUserId(userId).getId();
        session.setAttribute(ORDER_ID, orderId);
    }

    private void insertUserIdIntoOrderDB(HttpServletRequest req, long userId) {
        OrderServise orderServise = new OrderServiceImpl();
        orderServise.createOrder(userId);
        Order orderId = orderServise.getOrderByUserId(userId);
        req.getSession().setAttribute(ORDER_ID, orderId.getId());
    }
}
