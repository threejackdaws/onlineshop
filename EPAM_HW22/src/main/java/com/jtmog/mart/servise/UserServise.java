package com.jtmog.mart.servise;

import com.jtmog.mart.entity.User;

import javax.servlet.http.HttpServletRequest;

public interface UserServise {
    void createUser(String login);

    void createUser(String login, String password);

    User getUserByLogin(String login);

    void createUser(HttpServletRequest req, String clientName, String license);
}
