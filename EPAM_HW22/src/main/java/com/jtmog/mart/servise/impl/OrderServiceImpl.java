package com.jtmog.mart.servise.impl;

import com.jtmog.mart.dao.impl.OrderDAO;
import com.jtmog.mart.entity.Good;
import com.jtmog.mart.entity.Order;
import com.jtmog.mart.servise.OrderServise;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class OrderServiceImpl implements OrderServise {
    private static final Logger logger = Logger.getLogger(OrderServiceImpl.class);

    private static final String BUCKET = "bucket";
    private static final String USER_ID = "userId";
    private static final String TOTAL_PRICE = "totalPrice";

    @Override
    public void createOrder(long userId) {
        OrderDAO orderDAO = new OrderDAO();
        Order order = new Order(userId);
        orderDAO.insert(order);
    }

    @Override
    public Order getOrderByUserId(long userId) {
        Order order = null;
        OrderDAO orderDAO = new OrderDAO();
        order = orderDAO.read(userId);
        return order;
    }

    @Override
    public void updateOrder(long userId, long totalPrice) {
        Order order = new Order();
        OrderDAO orderDAO = new OrderDAO();
        order.setUserId(userId);
        order.setTotalPrice(totalPrice);
        orderDAO.update(order);
    }

    @Override
    public void updateTotalPriceInOrder(HttpServletRequest req) {
        List<Good> listGoodByOrderGood = (List<Good>) req.getAttribute(BUCKET);
        long totalPrice = 0;
        for (Good good : listGoodByOrderGood) {
            totalPrice += good.getPrice();
        }

        Long userId = (Long) req.getSession().getAttribute(USER_ID);

        updateOrder(userId, totalPrice);
    }

    @Override
    public void readTotalPriceFromOrder(HttpServletRequest req) {
        Long userId = (Long) req.getSession().getAttribute(USER_ID);
        OrderServise orderServise = new OrderServiceImpl();
        Order orderByUserId = orderServise.getOrderByUserId(userId);
        long totalPrice = orderByUserId.getTotalPrice();
        req.setAttribute(TOTAL_PRICE, totalPrice);
    }
}
