package com.jtmog.mart.servise;

import com.jtmog.mart.entity.OrderGood;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface OrderGoodServise {
    void createOrderGood(long orderId, long goodId);

    List<OrderGood> getAllGoodByOrderId(long orderId);

    void createOrderGood(HttpServletRequest req);

    void readOrderGoodByOrderId(HttpServletRequest req);
}
